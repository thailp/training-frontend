'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
  return gulp.src('resources/scss/**/*.scss')
    .pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer('last 2 version'))
  	.pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'));
});

gulp.task('sass:watch', function () {
  	gulp.watch('resources/scss/**/*.scss', ['sass']);
});